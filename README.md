# NASP - Napredni algoritmi i strukture podataka (Advanced Algorithms and Data Structures)
This folder contains solutions to laboratory exercises of Advanced Algorithms and Data Structures course.

## Content
* LAB01 - Red-Black Tree
* LAB02 - Text search (Rabin-Karp, Knuth-Morris-Pratt)
* LAB03 - Shortest path (Bellman-Ford)
